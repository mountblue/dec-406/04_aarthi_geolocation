var map;
  var place;
  var marker;
  var service;
  var placeLoc;
  var displace;
  var distance;
  var dicon;
//get the current location
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    alert("Geolocation is not supported by this browser.");
  }
}
//show the current location on map
function showPosition(position) {
    infowindow = new google.maps.InfoWindow();
    place = {lat: position.coords.latitude, lng: position.coords.longitude};
    map = new google.maps.Map(
      document.getElementById('googleMap'), {zoom: 15, center: place});
    marker=new google.maps.Marker({
      position: place,
      map: map,
      title: 'current location'
    });
    displace=document.getElementById("displace").value;
    distance=document.getElementById("distance").value;
    if(displace=="1"){
      dicon="http://maps.google.com/mapfiles/ms/icons/hospitals.png";
      displayPlaces('hospital');
    }
    else if(displace=="2"){
      dicon="http://maps.google.com/mapfiles/ms/icons/shopping.png";
      displayPlaces('store');
    }
    else if(displace=="3"){
      dicon="http://maps.google.com/mapfiles/ms/icons/restaurant.png";
      displayPlaces('restaurant');
    }
    else if(displace=="4"){
      dicon="http://maps.google.com/mapfiles/ms/icons/cabs.png";
      displayPlaces('taxi and car services');
    }
    else if(displace=="5"){
      dicon="http://maps.google.com/mapfiles/ms/icons/bus.png";
      displayPlaces('bus stop');
    }
    else if(displace=="6"){
      dicon="http://maps.google.com/mapfiles/ms/icons/movies.png";
      displayPlaces('theater');
    }
}
//display places
function displayPlaces(placeType){
  service = new google.maps.places.PlacesService(map);
      service.nearbySearch({
        location: place,
        radius: distance,
        type: [placeType]
      }, callback);
}
//maps places
function callback(results, status){
  if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }
        }
}
//marks places in map
function createMarker(place) {
        placeLoc = place.geometry.location;
        marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location,
          icon: dicon
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent(place.name);
          infowindow.open(map, this);
        });
      }